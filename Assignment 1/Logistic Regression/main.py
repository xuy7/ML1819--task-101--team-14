#!/usr/bin/python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model, preprocessing, metrics
from sklearn.model_selection import train_test_split, KFold, cross_val_score
from matplotlib.colors import ListedColormap
import tensorflow as tf
from numpy import mat
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import sklearn

import time

learning_rate = 0.001
training_epochs = 1000
k = 10

specy_dict = {'setosa': 0, 'versicolor' : 1, 'virginica' : 2}
color_dict = {0 : 'red', 1 : 'green', 2 : 'blue'}

tf_mse_history = []
pytorch_mse_history = []

sk_accuracy_history = []
tf_accuracy_history = []
pytorch_accuracy_history = []

index = np.arange(k)
sk_time_history = []
tf_time_history = []
pytorch_time_history = []

sk_loss_history = []
tf_loss_history = []
pytorch_loss_history = []

bar_width = 0.25

def sklearn_logistic_regression(X_train, X_test, Y_train, Y_test, k_fold):
    # encode label
    labelEncoder = preprocessing.LabelEncoder()
    Y_train_encoded = labelEncoder.fit_transform(Y_train.ravel())

    # logisticRegr = linear_model.LogisticRegression(max_iter=1000)
    # create SGD classifier with log loss function and learning reate and total epochs
    clf = linear_model.SGDClassifier(loss="log", eta0=learning_rate, max_iter=training_epochs)

    start_time = time.time()
    pervious_time = start_time
    # iterate X_train
    for train_index, test_index in k_fold.split(X_train):
        # train classifier
        clf.fit(X_train[train_index], Y_train_encoded[train_index])

        current_time = time.time()
        sk_time_history.append(current_time - pervious_time)
        pervious_time = current_time

        # record accuracy metrics
        sk_accuracy_history.append(clf.score(X_train[test_index], Y_train[test_index]))

        # record loss
        sk_loss_history.append(sklearn.metrics.log_loss(Y_train[test_index], clf.predict_proba(X_train[test_index])))
    # print(Y_train[test_index])
    # print(clf.predict(X_train[test_index]))

    Y_pred = clf.predict(X_test)

    # plt.scatter(X_test[:, 0], X_test[:, 1], color=np.where(Y_pred == 1, 'blue', 'red'))
    plt.figure(1)
    plt.suptitle('Scikit-Learn Logistic Regression')
    plt.scatter(X_test[:, 0], X_test[:, 1], color=pd.DataFrame(data={'species' : Y_pred}).replace({'species' : color_dict}).values.flatten())
    plt.xlabel('Sepal Length')
    plt.ylabel('Sepal Width')

def tensorflow_logistic_regression(X_train, X_test, Y_train, Y_test, k_fold):
    Y_specy_train = Y_train
    Y_specy_test = Y_test

    # hot encoding X and Y
    Y_train = np.zeros([len(Y_specy_train), 3])
    for index, specy in enumerate(Y_specy_train):
        Y_train[index][specy] = 1

    Y_test = np.zeros([len(Y_specy_test), 3])
    for index, specy in enumerate(Y_specy_test):
        Y_test[index][specy] = 1

    X = tf.placeholder(tf.float32, [None, 4]) # mnist data image of shape 28*28=784
    Y = tf.placeholder(tf.float32, [None, 3]) # 0-9 digits recognition => 10 classes

    W = tf.Variable(tf.zeros([4, 3]))
    b = tf.Variable(tf.zeros([3]))

    Y_pred = tf.nn.softmax(tf.matmul(X, W) + b)
    cost = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(Y_pred), reduction_indices=1))

    # X1_plot = ((-1) * (W[0] * X[:, 0] + W[2] * X[:, 2] + W[3] * X[:, 3] + b)) / W[1]

    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    accuracy, update_op = tf.metrics.accuracy(labels=tf.argmax(Y, 1), predictions=tf.argmax(Y_pred, 1))

    # Start training
    with tf.Session() as sess:
        # Run the initializer
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())

        cost_history = []
        accuracy_test_history = []
        accuracy_train_history = []

        start_time = time.time()
        pervious_time = start_time
        # iterate training data
        for train_index, test_index in k_fold.split(X_train):
            # iterate each epoch
            for epoch in range(training_epochs):
                # training
                sess.run(optimizer, feed_dict={X: X_train[train_index], Y: Y_train[train_index]})

                # record loss metrics
                tf_loss_history.append(sess.run(cost, feed_dict={X: X_train[train_index], Y: Y_train[train_index]}))

                # record accuracy
                sess.run([accuracy, update_op], feed_dict={X: X_train[test_index], Y: Y_train[test_index]})
                tf_accuracy_history.append(sess.run([accuracy], feed_dict={X: X_train[test_index], Y: Y_train[test_index]}))

                # sess.run([accuracy, update_op], feed_dict={X: X_train[train_index], Y: Y_train[train_index]})
                # accuracy_train_history.append(sess.run([accuracy], feed_dict={X: X_train[train_index], Y: Y_train[train_index]}))

            # record time cost
            current_time = time.time()
            tf_time_history.append(current_time - pervious_time)
            pervious_time = current_time

        # for epoch in range(training_epochs):
        #     sess.run(optimizer, feed_dict={X: X_train, Y: Y_train})
        #     tf_mse_history.append(sess.run(cost, feed_dict={X: X_test, Y: Y_test}))
        #
        #     sess.run([accuracy, update_op], feed_dict={X: X_test, Y: Y_test})
        #     tf_accuracy_history.append(sess.run([accuracy], feed_dict={X: X_test, Y: Y_test}))

        plt.figure(2)
        plt.suptitle('TensorFlow Logistic Regression')
        plt.scatter(X_test[:, 0], X_test[:, 1], color=pd.DataFrame(data={'species' : sess.run(tf.argmax(Y_pred, 1), feed_dict={X: X_test, Y: Y_test})}).replace({'species': color_dict}).values.flatten())
        plt.xlabel('Sepal Length')
        plt.ylabel('Sepal Width')

def pytorch_logistic_regression(X_train, X_test, Y_train, Y_test, k_fold):
    # Changing to array
    X_train = np.asarray(X_train,dtype=np.float32)
    Y_train = np.asarray(Y_train,dtype=np.float32)
    X_test = np.asarray(X_test,dtype=np.float32)
    Y_test = np.asarray(Y_test,dtype=np.float32)

    # Declaration
    class LogisticRegressionModel(nn.Module):

        def __init__(self, input_dim, output_dim):
            super(LogisticRegressionModel, self).__init__()
            self.linear = nn.Linear(input_dim, output_dim)

        def forward(self, x):
            out = self.linear(x)
            return out

    # Dimension decide
    input_dim = 4
    output_dim = 3

    # Model creation
    model = LogisticRegressionModel(input_dim, output_dim)
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate)

    # Training
    start_time = time.time()
    pervious_time = start_time
    # iterate train data with cross validation
    for train_index, test_index in k_fold.split(X_train):
        for epoch in range(training_epochs):

            # Declaring Variables
            X = Variable(torch.from_numpy(X_train[train_index]))
            Y = Variable(torch.from_numpy(Y_train[train_index].astype(np.int64)))

            #grads initializer
            optimizer.zero_grad()

            #forward to get predicted values
            optimizer.zero_grad()
            outputs = model(X)
            # loss = criterion(outputs, torch.max(Y, 1)[1])
            loss = criterion(outputs, Y)
            loss.backward()
            optimizer.step()

            X_t = Variable(torch.from_numpy(X_train[test_index]))
            Y_t = Variable(torch.from_numpy(Y_train[test_index].astype(np.int64)))

            pytorch_loss_history.append(loss.item())

            # _, predicted_t = torch.max(outputs.data, 1)
            _, predicted_t = torch.max(model.forward(X_t), 1)
            accuracy = torch.sum(Y_t == predicted_t).data.numpy() / Y_test.size
            pytorch_accuracy_history.append(accuracy)

        # record time cost
        current_time = time.time()
        pytorch_time_history.append(current_time - pervious_time)
        pervious_time = current_time

    # Predicting the values
    Y_pred = np.argmax(model.forward(Variable(torch.from_numpy(X_test))).data.numpy(), axis=1)

    plt.figure(3)
    plt.suptitle('PyTorchFlow Logistic Regression')
    plt.scatter(X_test[:, 0], X_test[:, 1], color=pd.DataFrame(data={'species' : Y_pred}).replace({'species' : color_dict}).values.flatten())
    plt.xlabel('Sepal Length')
    plt.ylabel('Sepal Width')

def main():
    # load data
    dataset = pd.read_csv('./Dataset/iris.csv')

    # get features from sepal length, sepal width, petal length and petal width columns
    X_dataset = dataset[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']].values
    # get labels from species column
    Y_dataset = dataset.replace({'species': specy_dict})['species'].values

    # Train test split
    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, test_size=0.1)

    # create k fold with k
    k_fold = KFold(n_splits=k, shuffle=True)
    # split dataset into k fold
    k_fold.get_n_splits(X_train)

    # run logistic regression with sklearn
    sklearn_logistic_regression(X_train, X_test, Y_train, Y_test, k_fold)

    # run logistic regression with tensorflow
    tensorflow_logistic_regression(X_train, X_test, Y_train, Y_test, k_fold)

    # run logistic regression with pytorch
    pytorch_logistic_regression(X_train, X_test, Y_train, Y_test, k_fold)

    # present loss diagram
    plt.figure(4)
    plt.suptitle('Training Loss')
    plt.plot(range(1, training_epochs * k + 1, training_epochs), sk_loss_history, color='blue')
    plt.plot(range(1, training_epochs * k + 1), tf_loss_history, color='orange')
    plt.plot(range(1, training_epochs * k + 1), pytorch_loss_history, color='red')
    plt.grid(True)
    plt.xlabel('Training')
    plt.ylabel('Loss')
    plt.legend(('Scikit-Learn', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    # present accuracy diagram
    plt.figure(5)
    plt.suptitle('Training Test Accuracy')
    plt.plot(range(1, training_epochs * k + 1, training_epochs), sk_accuracy_history, color='blue')
    plt.plot(range(1, training_epochs * k + 1), tf_accuracy_history, color='orange')
    plt.plot(range(1, training_epochs * k + 1), pytorch_accuracy_history, color='red')
    plt.grid(True)
    plt.xlabel('Training')
    plt.ylabel('Accuracy')
    plt.legend(('Scikit-Learn', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    # present time cost diagram
    plt.figure(6)
    plt.suptitle('Training Time Comsumption')

    plt.bar(index, sk_time_history, bar_width, label="Scikit-Learn")
    # plt.plot(index, sk_time_history, alpha=0.5, color='blue')
    # plt.scatter(index, sk_time_history, alpha=0.5, s=20, color='blue')

    plt.bar(index +  bar_width, tf_time_history, bar_width, label="TensorFlow")
    # plt.plot(index +  bar_width, tf_time_history, alpha=0.5, color='orange')
    # plt.scatter(index +  bar_width, tf_time_history, alpha=0.5, s=20, color='orange')

    plt.bar(index +  2 * bar_width, pytorch_time_history, bar_width, label="PyTorch")
    # plt.plot(index +  2 * bar_width, pytorch_time_history, alpha=0.5, color='red')
    # plt.scatter(index +  2 * bar_width, pytorch_time_history, alpha=0.5, s=20, color='red')

    plt.xlabel('Dataset')
    plt.ylabel('Time')

    plt.grid(True)

    plt.legend(('Scikit-Learn', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    plt.show()

if __name__ == '__main__':
    main()
