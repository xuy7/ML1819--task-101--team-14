#!/usr/bin/python2

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model, preprocessing, metrics
from sklearn.model_selection import train_test_split, KFold
from matplotlib.colors import ListedColormap

if __name__ == '__main__':
    learning_rate = 0.01
    training_epochs = 100
    k = 10

    specy_dict = {'setosa': 0, 'versicolor' : 1, 'virginica' : 2}
    color_dict = {0 : 'red', 1 : 'green', 2 : 'blue'}

    dataset = pd.read_csv('../Dataset/iris.csv')

    X_dataset = dataset[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']].values
    Y_dataset = dataset.replace({'species': specy_dict})['species'].values

    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, test_size=0.1)

    # Encode
    labelEncoder = preprocessing.LabelEncoder()
    Y_train_encoded = labelEncoder.fit_transform(Y_train.ravel())

    k_fold = KFold(n_splits=k, shuffle=True)
    k_fold.get_n_splits(X_train)

    # logisticRegr = linear_model.LogisticRegression(max_iter=1000)
    clf = linear_model.SGDClassifier(loss="log", eta0=learning_rate, max_iter=training_epochs)

    for train_index, test_index in k_fold.split(X_train):
        clf.fit(X_train[train_index], Y_train_encoded[train_index])

    Y_pred = clf.predict(X_train)

    # plt.scatter(X_test[:, 0], X_test[:, 1], color=np.where(Y_pred == 1, 'blue', 'red'))
    plt.scatter(X_train[:, 0], X_train[:, 1], color=pd.DataFrame(data={'species' : Y_pred}).replace({'species' : color_dict}).values.flatten())
    plt.xlabel('Sepal Length')
    plt.ylabel('Sepal Width')

    plt.xticks(())
    plt.yticks(())

    plt.show()
