#!/usr/bin/python3

# Importing Libraries
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch.nn.functional as F
from sklearn.model_selection import train_test_split

if __name__ == '__main__':
    specy_dict = {'setosa': 0, 'versicolor' : 1, 'virginica' : 2}
    color_dict = {0 : 'red', 1 : 'green', 2 : 'blue'}

    dataset = pd.read_csv('../Dataset/iris.csv')

    X_dataset = dataset[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']].values
    Y_dataset = dataset.replace({'species': specy_dict})['species'].values

    # Train test split
    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, test_size=0.1)

    # Changing to array
    X_train = np.asarray(X_train,dtype=np.float32)
    Y_train = np.asarray(Y_train,dtype=np.float32)
    X_test = np.asarray(X_test,dtype=np.float32)
    Y_test = np.asarray(Y_test,dtype=np.float32)

    # Declaration
    class LogisticRegressionModel(nn.Module):

        def __init__(self, input_dim, output_dim):
            super(LogisticRegressionModel, self).__init__()
            self.linear = nn.Linear(input_dim, output_dim)

        def forward(self, x):
            out = self.linear(x)
            return out

    # Dimension decide
    input_dim = 4
    output_dim = 3

    # Model creation
    model = LogisticRegressionModel(input_dim, output_dim)
    criterion = nn.CrossEntropyLoss()
    learning_rate = 0.01
    optimizer = torch.optim.SGD(model.parameters(), lr = learning_rate)
    training_epochs = 10000

    # Training
    for epoch in range(training_epochs):

        # Declaring Variables
        X = Variable(torch.from_numpy(X_train))
        Y = Variable(torch.from_numpy(Y_train.astype(np.int64)))

        #grads initializer
        optimizer.zero_grad()

        #forward to get predicted values
        optimizer.zero_grad()
        outputs = model(X)
        # loss = criterion(outputs, torch.max(Y, 1)[1])
        loss = criterion(outputs, Y)
        loss.backward()
        optimizer.step()

        # _, predicted_t = torch.max(outputs.data, 1)

    # Predicting the values
    Y_pred = np.argmax(model.forward(Variable(torch.from_numpy(X_train))).data.numpy(), axis=1)

    plt.scatter(X_train[:, 0], X_train[:, 1], color=pd.DataFrame(data={'species' : Y_pred}).replace({'species' : color_dict}).values.flatten())
    plt.xlabel('Sepal Length')
    plt.ylabel('Sepal Width')

    plt.xticks(())
    plt.yticks(())

    plt.show()
