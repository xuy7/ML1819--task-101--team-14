#!/usr/bin/python3

import tensorflow as tf
import pandas as pd
import numpy as np
from numpy import mat
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

if __name__ == '__main__':
    learning_rate = 0.01
    training_epochs = 10000

    specy_dict = {'setosa': 0, 'versicolor' : 1, 'virginica' : 2}
    color_dict = {0 : 'red', 1 : 'green', 2 : 'blue'}

    dataset = pd.read_csv('../Dataset/iris.csv')

    X_dataset = dataset[['sepal_length', 'sepal_width', 'petal_length', 'petal_width']].values
    Y_specy = dataset.replace({'species': specy_dict})['species'].values

    Y_dataset = np.zeros([len(Y_specy), 3])
    for index, specy in enumerate(Y_specy):
        Y_dataset[index][specy] = 1

    X_train = X_dataset
    Y_train = Y_dataset

    # X1_min = np.amin(X_train[:0])
    # X1_max = np.amax(X_train[:1])

    X = tf.placeholder(tf.float32, [None, 4]) # mnist data image of shape 28*28=784
    Y = tf.placeholder(tf.float32, [None, 3]) # 0-9 digits recognition => 10 classes

    W = tf.Variable(tf.zeros([4, 3]))
    b = tf.Variable(tf.zeros([3]))

    Y_pred = tf.nn.softmax(tf.matmul(X, W) + b)
    cost = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(Y_pred), reduction_indices=1))

    # X1_plot = ((-1) * (W[0] * X[:, 0] + W[2] * X[:, 2] + W[3] * X[:, 3] + b)) / W[1]

    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    accuracy, update_op = tf.metrics.accuracy(labels=tf.argmax(Y, 1), predictions=tf.argmax(Y_pred, 1))

    # Start training
    with tf.Session() as sess:
        # Run the initializer
        sess.run(tf.local_variables_initializer())
        sess.run(tf.global_variables_initializer())

        cost_history = []
        accuracy_history = []
        for epoch in range(training_epochs):
            sess.run(optimizer, feed_dict={X: X_train, Y: Y_train})
            cost_history.append(sess.run(cost, feed_dict={X: X_train, Y: Y_train}))

            sess.run([accuracy, update_op], feed_dict={X: X_train, Y: Y_train})
            accuracy_history.append(sess.run([accuracy], feed_dict={X: X_train, Y: Y_train}))

        plt.figure(1)
        plt.scatter(X_train[:, 0], X_train[:, 1], color=pd.DataFrame(data={'species' : sess.run(tf.argmax(Y_pred, 1), feed_dict={X: X_train, Y: Y_train})}).replace({'species': color_dict}).values.flatten())
        plt.xlabel('Sepal Length')
        plt.ylabel('Sepal Width')

        plt.figure(2)
        plt.plot(range(1, training_epochs+1), cost_history)
        plt.xlabel('Train')
        plt.ylabel('Cost')

        plt.figure(3)
        plt.plot(range(1, training_epochs+1), accuracy_history)
        plt.xlabel('Train')
        plt.ylabel('Accuracy')

        plt.show()
