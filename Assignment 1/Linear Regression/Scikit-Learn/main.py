#!/usr/bin/python2

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import KFold, train_test_split, learning_curve, ShuffleSplit, cross_validate
from sklearn.metrics import mean_absolute_error

def main():
    learning_rate = 0.0001
    training_epochs = 100
    k = 10

    # Training Data
    # train_dataset = pd.read_csv('../Dataset/train.csv')
    # test_dataset = pd.read_csv('../Dataset/test.csv')

    dataset = pd.read_csv('../Dataset/weights_heights.csv')

    X_dataset = dataset[['Height']].values
    Y_dataset = dataset['Weight'].values

    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, shuffle=True, test_size=0.1)

    # k_fold = KFold(n_splits=10, shuffle=True, random_state=None)
    # k_fold.get_n_splits(X_train)

    regr = linear_model.SGDRegressor(loss="squared_loss", eta0=learning_rate, max_iter=training_epochs)
    # regr = linear_model.LinearRegression()

    # for train_index, test_index in k_fold.split(X_train):
    #     # print(regr.score(X_test, y_test))
    #     X_train_batch = X_train[train_index]
    #     Y_train_batch = Y_train[train_index]
    #     X_test_batch = X_train[test_index]
    #     Y_test_batch = Y_train[test_index]
    #     regr.fit(X_train_batch, Y_train_batch)
    #     # print(regr.score(X_test_batch, Y_test_batch))
    #     # print(accuracy_score(Y_test_batch, regr.predict(X_test_batch)))
    #     print(mean_absolute_error(Y_test_batch, regr.predict(X_test_batch)))

    # regr.fit(X_train, Y_train)

    # cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
    # train_sizes, train_scores, test_scores = learning_curve(regr, X_train, Y_train, cv=cv, n_jobs=4)

    cv_results = cross_validate(regr, X_train, Y_train, cv=k, scoring=('neg_mean_absolute_error', 'neg_mean_squared_error'), return_train_score=True)

    print(cv_results)

    # Y_pred = regr.predict(X_train)
    # plt.plot(X_test, Y_test, 'ro', color='red')
    # plt.plot(X_train, Y_pred, color='blue')
    # print(mean_absolute_error(Y_test, Y_pred))

    plt.figure(1)
    plt.plot(range(1, k + 1), cv_results['test_neg_mean_squared_error'])
    plt.plot(range(1, k + 1), cv_results['train_neg_mean_squared_error'])

    plt.show()

    # print(cv_results['neg_mean_absolute_error'])

if __name__ == '__main__':
    main()
