#!/usr/bin/python3

import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.utils.data
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import KFold, train_test_split, learning_curve, ShuffleSplit, cross_validate
import tensorflow as tf
import sklearn

import time

learning_rate = 0.0001
training_epochs = 100
k = 10

sk_mse_history = []
tf_mse_history = []
pytorch_mse_history = []

sk_mae_history = []
tf_mae_history = []
pytorch_mae_history = []

tf_cost_history = []
pytorch_cost_history = []

sk_time_history = []
tf_time_history = []
pytorch_time_history = []

index = np.arange(1, k+1)

bar_width = 0.25

def sklearn_linear_regression(X_train, X_test, Y_train, Y_test, k_fold):

    # use SGD Regressor mode with MSE loss function
    regr = linear_model.SGDRegressor(loss="squared_loss", eta0=learning_rate, max_iter=training_epochs)

    start_time = time.time()
    pervious_time = start_time

    # iterate k fold of dataset
    for train_index, test_index in k_fold.split(X_train):
        # train SGD regressor
        regr.fit(X_train[train_index], Y_train[train_index].ravel())

        current_time = time.time()
        sk_time_history.append(current_time - pervious_time)
        pervious_time = current_time

        # get MSE metric of each epoch
        sk_mse_history.append(sklearn.metrics.mean_squared_error(Y_train[test_index].ravel(), regr.predict(X_train[test_index])))

    sk_mse = sklearn.metrics.mean_squared_error(Y_test.ravel(), regr.predict(X_test))

    # show present prediction in the diagram
    plt.figure(1)
    plt.suptitle('Scikit-Learn Linear Regression')
    Y_pred = regr.predict(X_train)
    plt.plot(X_test, Y_test, 'ro', alpha = 0.5, color='red')
    plt.plot(X_train, Y_pred, alpha = 0.5, color='blue')

    plt.xlabel('Height')
    plt.ylabel('Weight')

def tensorflow_linear_regression(X_train, X_test, Y_train, Y_test, k_fold):
    # k_fold = KFold(n_splits=k, shuffle=True, random_state=None)
    # k_fold.get_n_splits(X_train)

    # tf Graph Input
    X = tf.placeholder(tf.float32, [None, 1])
    Y = tf.placeholder(tf.float32, [None, 1])

    # Set model weights
    W = tf.Variable(tf.zeros([1, 1]), name="weight")
    b = tf.Variable(tf.zeros([1]), name="bias")

    # Construct a linear model
    # Y_pred = tf.add(tf.multiply(X, W), b)
    Y_pred = tf.add(tf.matmul(X, W), b)

    # cost function
    cost = tf.reduce_mean(tf.square(tf.subtract(Y_pred, Y)))

    # mean absolute error
    mean_absolute_error, update_op = tf.metrics.mean_absolute_error(Y, Y_pred)

    # mean squared error
    mean_squared_error, update_op = tf.metrics.mean_squared_error(Y, Y_pred)

    # Gradient descent
    #  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    # Initialize the variables (i.e. assign their default value)
    init_global = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    # Start training
    with tf.Session() as sess:

        # sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        # Run the initializer
        sess.run(init_global)
        sess.run(init_local)

        # for epoch in range(training_epochs):
        # sess.run(optimizer, feed_dict={X: X_dataset, Y: Y_dataset})
        # sess.run(optimizer, feed_dict={X: X_dataset, Y: Y_dataset})

        # Fit all training data
        mae_test_history = []
        mae_train_history = []
        start_time = time.time()
        pervious_time = start_time

        # iterate k fold of dataset
        for train_index, test_index in k_fold.split(X_train):
            # iterate training epochs times of each fold
            for epoch in range(training_epochs):
                sess.run(optimizer, feed_dict={X: X_train[train_index], Y: Y_train[train_index]})

                # record mse metrics in each epoch
                tf_mse_history.append(sess.run(cost, feed_dict={X: X_train[train_index], Y: Y_train[train_index]}))

                # record mae metrics in each epoch
                sess.run([mean_absolute_error, update_op], feed_dict={X: X_train[test_index], Y: Y_train[test_index]})
                tf_mae_history.append(sess.run(mean_absolute_error, feed_dict={X: X_train[test_index], Y: Y_train[test_index]}))

            # record time cost in each epoch
            current_time = time.time()
            tf_time_history.append(current_time - pervious_time)
            pervious_time = current_time

        # for epoch in range(training_epochs):
        #     sess.run(optimizer, feed_dict={X: X_train, Y: Y_train})
        #
        #     tf_mse_history.append(sess.run(cost, feed_dict={X: X_train, Y: Y_train}))
            # sess.run([mean_absolute_error, update_op], feed_dict={X: X_test, Y: Y_test})
            # tf_mse_history.append(sess.run(mean_absolute_error))

        tf_mse = sess.run(cost, feed_dict={X: X_test, Y: Y_test})

        # present predictions in diagram
        plt.figure(2)
        plt.suptitle('TensorFlow Linear Regression')
        plt.plot(X_test, Y_test, 'ro',  alpha = 0.5)
        plt.plot(X_train, sess.run(Y_pred, feed_dict={X: X_train}), alpha = 0.5)

        plt.xlabel('Height')
        plt.ylabel('Weight')

def pytorch_linear_regression(X_train, X_test, Y_train, Y_test, k_fold):
        # Data set read
        # dataset =  pd.read_csv('../Dataset/weights_heights.csv')
        # dataset = dataset.drop(['Index'],axis=1)
        # X_dataset = dataset.iloc[:, :1]
        # Y_dataset = dataset.iloc[:,1:]
        # X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, test_size=0.1)
        X_train = np.asarray(X_train,dtype=np.float32).reshape(-1,1)
        Y_train = np.asarray(Y_train,dtype=np.float32).reshape(-1,1)
        X_test = np.asarray(X_test,dtype=np.float32).reshape(-1,1)
        Y_test = np.asarray(Y_test,dtype=np.float32).reshape(-1,1)

        # Declaration
        class LinearRegressionModel(nn.Module):
            def __init__(self, input_dim, output_dim):
                super(LinearRegressionModel, self).__init__()
                self.linear = nn.Linear(input_dim, output_dim)

            def forward(self, x):
                out = self.linear(x)
                return out

        input_dim = 1
        output_dim = 1

        #model creation
        model = LinearRegressionModel(input_dim, output_dim)
        criterion = nn.MSELoss()
        optimiser = torch.optim.SGD(model.parameters(), lr = learning_rate)

        #training the model
        start_time = time.time()
        pervious_time = start_time
        for train_index, test_index in k_fold.split(X_train):
            for epoch in range(training_epochs):

                X = Variable(torch.from_numpy(X_train[train_index]))
                Y = Variable(torch.from_numpy(Y_train[train_index]))

                X_t = Variable(torch.from_numpy(X_train[test_index]))
                Y_t = Variable(torch.from_numpy(Y_train[test_index]))

                # grads initializer
                optimiser.zero_grad()

                # forward to get predicted values
                outputs = model.forward(X)
                loss = criterion(outputs, Y)
                loss.backward()
                optimiser.step()

                # record mse metrics in each epoch
                pytorch_mse_history.append(loss.item())

            # record time cost in each epoch
            current_time = time.time()
            pytorch_time_history.append(current_time - pervious_time)
            pervious_time = current_time

        pytorch_mse = loss.item()

        plt.figure(3)
        plt.suptitle('PyTorch Linear Regression')
        predicted = model.forward(Variable(torch.from_numpy(X_train))).data.numpy()
        plt.plot(X_test, Y_test, 'go', label = 'from data', alpha = 0.5, color = 'red')
        plt.plot(X_train, predicted, label = 'prediction', alpha = 0.5, color = 'blue')

        plt.xlabel('Height')
        plt.ylabel('Weight')

def main():
    # load dataset
    dataset = pd.read_csv('Dataset/weights_heights.csv')

    # get height as feature
    X_dataset = dataset[['Height']].values
    # get weight as fact
    Y_dataset = dataset[['Weight']].values

    # split dataset into test data nd train data
    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, shuffle=True, test_size=0.1)

    # create k fold object with k = 10
    k_fold = KFold(n_splits=k, shuffle=True, random_state=None)
    # spit train data with k fold object
    k_fold.get_n_splits(X_train)

    # run linear regression with sklearn
    sklearn_linear_regression(X_train, X_test, Y_train, Y_test, k_fold)

    # run linear regression with tensorflow
    tensorflow_linear_regression(X_train, X_test, Y_train, Y_test, k_fold)

    # run linear regression with pytorch
    pytorch_linear_regression(X_train, X_test, Y_train, Y_test, k_fold)

    # show MSE diagram
    plt.figure(4)
    plt.suptitle('MSE')
    plt.plot(range(1, k * training_epochs + 1, training_epochs), sk_mse_history, alpha = 0.5)
    plt.plot(range(1, k * training_epochs + 1), tf_mse_history, alpha = 0.5)
    plt.plot(range(1, k * training_epochs + 1), pytorch_mse_history, alpha = 0.5)
    plt.grid(True)
    plt.legend(('Scikit-Learn', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    # show time cost diagram
    plt.figure(5)
    plt.suptitle('Training Time Comsumption')

    plt.bar(index, sk_time_history, bar_width, label="Scikit-Learn")
    # plt.plot(index, sk_time_history, alpha=0.5, color='blue')
    # plt.scatter(index, sk_time_history, alpha=0.5, s=20, color='blue')

    plt.bar(index +  bar_width, tf_time_history, bar_width, label="TensorFlow")
    # plt.plot(index +  bar_width, tf_time_history, alpha=0.5, color='orange')
    # plt.scatter(index +  bar_width, tf_time_history, alpha=0.5, s=20, color='orange')

    plt.bar(index +  2 * bar_width, pytorch_time_history, bar_width, label="PyTorch")
    # plt.plot(index +  2 * bar_width, pytorch_time_history, alpha=0.5, color='red')
    # plt.scatter(index +  2 * bar_width, pytorch_time_history, alpha=0.5, s=20, color='red')

    plt.xlabel('Dataset')
    plt.ylabel('Time')

    plt.grid(True)

    plt.legend(('Scikit-Learn', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    plt.show()

if __name__ == '__main__':
    main()
