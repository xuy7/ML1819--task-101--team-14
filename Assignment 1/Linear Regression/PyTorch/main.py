#!/usr/bin/python3

# Importing Libraries
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import torch.utils.data
from sklearn.model_selection import train_test_split

if __name__ == '__main__':
    learning_rate = 0.0001
    training_epochs = 100

    # Data set read
    dataset =  pd.read_csv('../Dataset/weights_heights.csv')
    dataset = dataset.drop(['Index'],axis=1)
    X_dataset = dataset.iloc[:, :1]
    Y_dataset = dataset.iloc[:,1:]
    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, test_size=0.1)
    X_train = np.asarray(X_train,dtype=np.float32).reshape(-1,1)
    Y_train = np.asarray(Y_train,dtype=np.float32).reshape(-1,1)
    X_test = np.asarray(X_test,dtype=np.float32).reshape(-1,1)
    Y_test = np.asarray(Y_test,dtype=np.float32).reshape(-1,1)

    # Declaration
    class LinearRegressionModel(nn.Module):

        def __init__(self, input_dim, output_dim):
            super(LinearRegressionModel, self).__init__()
            self.linear = nn.Linear(input_dim, output_dim)

        def forward(self, x):
            out = self.linear(x)
            return out

    input_dim = 1
    output_dim = 1

    #model creation
    model = LinearRegressionModel(input_dim,output_dim)
    criterion = nn.MSELoss()
    optimiser = torch.optim.SGD(model.parameters(), lr = learning_rate)

    #training the model
    loss_history = []
    for epoch in range(training_epochs):

        X = Variable(torch.from_numpy(X_train))
        Y = Variable(torch.from_numpy(Y_train))

        #grads initializer
        optimiser.zero_grad()

        #forward to get predicted values
        outputs = model.forward(X)
        loss = criterion(outputs, Y)
        loss.backward()
        optimiser.step()
        loss_history.append(loss.item())

    # Predicting the values
    plt.figure(1)
    predicted = model.forward(Variable(torch.from_numpy(X_train))).data.numpy()
    plt.plot(X_test, Y_test, 'go', label = 'from data', alpha = 0.5, color = 'b')
    plt.plot(X_train, predicted, label = 'prediction', alpha = 0.5, color = 'r')

    plt.figure(2)
    plt.plot(range(1, training_epochs + 1), loss_history, label = 'prediction', alpha = 0.5, color = 'r')

    plt.legend()
    plt.show()
