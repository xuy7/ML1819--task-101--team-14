#!/usr/bin/python3

import tensorflow as tf
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from tensorflow.metrics import mean_absolute_error
from sklearn.model_selection import KFold, train_test_split
from tensorflow.metrics import mean_absolute_error

if __name__ == '__main__':
    # Parameters
    learning_rate = 0.00001
    training_epochs = 10
    k = 10

    # Training Data
    dataset = pd.read_csv('../Dataset/train.csv')
    X_dataset = dataset[['x']].values.astype(np.float32)
    Y_dataset = dataset[['y']].values.astype(np.float32)

#     X_dataset = np.asarray([[3.3],[4.4],[5.5],[6.71],[6.93],[4.168],[9.779],[6.182],[7.59],[2.167],
# [7.042],[10.791],[5.313],[7.997],[5.654],[9.27],[3.1]]).astype(np.float32)
#     Y_dataset = np.asarray([[1.7],[2.76],[2.09],[3.19],[1.694],[1.573],[3.366],[2.596],[2.53],[1.221],
# [2.827],[3.465],[1.65],[2.904],[2.42],[2.94],[1.3]]).astype(np.float32)

    X_train, X_test, Y_train, Y_test = train_test_split(X_dataset, Y_dataset, test_size=0.1)

    k_fold = KFold(n_splits=k, shuffle=True, random_state=None)
    k_fold.get_n_splits(X_train)

    # tf Graph Input
    X = tf.placeholder(tf.float32, [None, 1])
    Y = tf.placeholder(tf.float32, [None, 1])

    # Set model weights
    W = tf.Variable(tf.zeros([1, 1]), name="weight")
    b = tf.Variable(tf.zeros([1]), name="bias")

    # Construct a linear model
    # Y_pred = tf.add(tf.multiply(X, W), b)
    Y_pred = tf.add(tf.matmul(X, W), b)

    # cost function
    cost = tf.reduce_mean(tf.square(tf.subtract(Y_pred, Y)))

    # mean absolute error
    mean_absolute_error, update_op = mean_absolute_error(Y, Y_pred)

    # Gradient descent
    #  Note, minimize() knows to modify W and b because Variable objects are trainable=True by default
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    # Initialize the variables (i.e. assign their default value)
    init = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    # Start training
    with tf.Session() as sess:

        # sess = tf_debug.LocalCLIDebugWrapperSession(sess)

        # Run the initializer
        sess.run(init)
        sess.run(init_local)

        # for epoch in range(training_epochs):
        # sess.run(optimizer, feed_dict={X: X_dataset, Y: Y_dataset})
        # sess.run(optimizer, feed_dict={X: X_dataset, Y: Y_dataset})

        # Fit all training data
        cost_history = []
        mae_test_history = []
        mae_train_history = []
        for train_index, test_index in k_fold.split(X_train):
            for epoch in range(training_epochs):
                sess.run(optimizer, feed_dict={X: X_train[train_index], Y: Y_train[train_index]})

                cost_history.append(sess.run(cost, feed_dict={X: X_train[train_index], Y: Y_train[train_index]}))

                sess.run([mean_absolute_error, update_op], feed_dict={X: X_train[test_index], Y: Y_train[test_index]})
                mae_train_history.append(sess.run(mean_absolute_error, feed_dict={X: X_train[train_index], Y: Y_train[train_index]}))

                sess.run([mean_absolute_error, update_op], feed_dict={X: X_train[test_index], Y: Y_train[test_index]})
                mae_test_history.append(sess.run(mean_absolute_error, feed_dict={X: X_train[test_index], Y: Y_train[test_index]}))

        print("Optimization Finished!")

        # Graphic display
        plt.figure(1)
        plt.suptitle('Prediction')
        plt.plot(X_test, Y_test, 'ro', label='Original data')
        plt.plot(X_test, sess.run(Y_pred, feed_dict={X: X_test}))

        plt.figure(2)
        plt.suptitle('MAE')
        plt.plot(range(1, k * training_epochs + 1), mae_train_history, color='green')
        plt.plot(range(1, k * training_epochs + 1), mae_test_history, color='blue')

        plt.figure(3)
        plt.suptitle('Cost')
        plt.plot(range(1, k * training_epochs + 1), cost_history)

        plt.show()
