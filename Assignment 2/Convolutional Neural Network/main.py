from __future__ import print_function
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
import matplotlib.pyplot as plt

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("../data", one_hot=False)

import tensorflow as tf

import keras
import keras.datasets
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

import numpy as np

import time

# Training hyperparameters
num_epochs = 10
batch_size = 100
learning_rate = 0.01
momentum = 0.5
log_interval = 20
dropout=0
num_classes = 10

n_train = 60000
n_test = 10000

keras_losses = []
tf_losses = []
pytorch_losses = []

keras_accuracies = []
tf_accuracies = []
pytorch_accuracies = []

keras_times = []
tf_times = []
pytorch_times = []

class LoggingCallback(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.batch_idx = 0
        self.start_time = 0
        self.end_time = 0
        return

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        self.start_time = time.time();
        return

    def on_epoch_end(self, epoch, logs={}):
        self.end_time = time.time()
        keras_times.append(self.end_time - self.start_time)
        return

    def on_batch_begin(self, batch, logs={}):
        self.batch_idx += 1
        return

    def on_batch_end(self, batch, logs={}):
        if(self.batch_idx % log_interval == 0):
            loss = logs.get('loss')
            accuracy = logs.get('acc') * 100.
            # print("Loss: ", loss)
            # print("Accuracy: ", accuracy)
            keras_losses.append(loss)
            keras_accuracies.append(accuracy)
        return

def train_keras():
    img_rows, img_cols = 28, 28

    logging_callback = LoggingCallback()

    # the data, split between train and test sets
    (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255
    print('x_train shape:', x_train.shape)
    print(x_train.shape[0], 'train samples')
    print(x_test.shape[0], 'test samples')

    # convert class vectors to binary class matrices
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    model = Sequential()
    model.add(Conv2D(16, kernel_size=(5, 5),
                     activation='relu',
                     input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=num_epochs,
              verbose=1,
              validation_data=(x_test, y_test),
              callbacks=[logging_callback])
    score = model.evaluate(x_test, y_test, verbose=0)
    # print('Test loss:', score[0])
    # print('Test accuracy:', score[1])

class LoggingHook(tf.train.SessionRunHook):
    def __init__(self, loss, acc):
        self.loss = loss
        self.acc = acc
        self.batch_idx = 0
        self.start_time = 0
        self.end_time = 0
        self.is_first = True
        self.epoch = 0

    def begin(self):
        pass

    def before_run(self, run_context):
        self.batch_idx += 1;
        if self.is_first:
            self.is_first = False
            self.start_time = time.time()
        return tf.train.SessionRunArgs([self.loss, self.acc])

    def after_run(self, run_context, run_values):
        # if self.batch_idx % log_interval == 0:
        #     loss_value = run_values.results
        #     print('Train: [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
        #         self.batch_idx, 60000, (100. * self.batch_idx / 60000), loss_value
        #     ))
        if self.batch_idx % log_interval == 0:
            loss = run_values.results[0]
            accuracy = run_values.results[1] * 100.
            print('Train: [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                self.batch_idx * batch_size / num_epochs, n_train, (100. * self.batch_idx * batch_size * self.epoch / 60000), loss
            ))
            tf_losses.append(loss)
            tf_accuracies.append(accuracy)

        if(self.batch_idx % (n_train / batch_size) == 0):
            self.end_time = time.time()
            tf_times.append(self.end_time - self.start_time)
            self.start_time = time.time()
            self.epoch += 1

# Create the neural network
def conv_net(x_dict, n_classes, dropout, reuse, is_training):
    # Define a scope for reusing the variables
    with tf.variable_scope('ConvNet', reuse=reuse):
        # TF Estimator input is a dict, in case of multiple inputs
        x = x_dict['images']

        # MNIST data input is a 1-D vector of 784 features (28*28 pixels)
        # Reshape to match picture format [Height x Width x Channel]
        # Tensor input become 4-D: [Batch Size, Height, Width, Channel]
        x = tf.reshape(x, shape=[-1, 28, 28, 1])

        # Convolution Layer with 32 filters and a kernel size of 4
        conv1 = tf.layers.conv2d(x, 16, 4, activation=tf.nn.relu)
        # Max Pooling (down-sampling) with strides of 1 and kernel size of 4
        conv1 = tf.layers.max_pooling2d(conv1, 1, 4)

        # # Convolution Layer with 64 filters and a kernel size of 3
        # conv2 = tf.layers.conv2d(conv1, 64, 3, activation=tf.nn.relu)
        # # Max Pooling (down-sampling) with strides of 2 and kernel size of 2
        # conv2 = tf.layers.max_pooling2d(conv2, 2, 2)

        # Flatten the data to a 1-D vector for the fully connected layer
        fc1 = tf.contrib.layers.flatten(conv1)

        # Fully connected layer (in tf contrib folder for now)
        fc1 = tf.layers.dense(fc1, 128, activation=tf.nn.relu)
        # Apply Dropout (if is_training is False, dropout is not applied)
        # fc1 = tf.layers.dropout(fc1, rate=dropout, training=is_training)

        # Fully connected layer (in tf contrib folder for now)
        fc2 = tf.layers.dense(fc1, 10, activation=tf.nn.log_softmax)

        # Output layer, class prediction
        out = fc2

    return out

# Define the model function (following TF Estimator Template)
def model_fn(features, labels, mode):
    # Build the neural network
    # Because Dropout have different behavior at training and prediction time, we
    # need to create 2 distinct computation graphs that still share the same weights.
    logits_train = conv_net(features, 10, dropout, reuse=False,
                            is_training=True)
    logits_test = conv_net(features, 10, dropout, reuse=True,
                           is_training=False)

    # Predictions
    pred_classes = tf.argmax(logits_test, axis=1)
    pred_probas = tf.nn.softmax(logits_test)

    # If prediction mode, early return
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=pred_classes)

    # Define loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logits_train, labels=tf.cast(labels, dtype=tf.int32)))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op,
                                  global_step=tf.train.get_global_step())

    # Evaluate the accuracy of the model
    accuracy, acc_op = tf.metrics.accuracy(labels=labels, predictions=pred_classes)

    logging_hook = LoggingHook(loss_op, acc_op)
    # logging_hook = tf.train.LoggingTensorHook({"loss": loss_op}, every_n_iter=1);

    # TF Estimators requires to return a EstimatorSpec, that specify
    # the different ops for training, evaluating, ...
    estim_specs = tf.estimator.EstimatorSpec(
        mode=mode,
        predictions=pred_classes,
        loss=loss_op,
        train_op=train_op,
        eval_metric_ops={'accuracy': (accuracy, acc_op)},
        training_hooks=[logging_hook])

    return estim_specs

def train_tensorflow():

    # Build the Estimator
    model = tf.estimator.Estimator(model_fn)

    # Define the input function for training
    input_fn = tf.estimator.inputs.numpy_input_fn(
        x={'images': np.concatenate((mnist.train.images, mnist.validation.images), axis=0)}, y=np.concatenate((mnist.train.labels, mnist.validation.labels)),
        batch_size=batch_size, num_epochs=num_epochs, shuffle=True)

    # Train the Model
    model.train(input_fn)

    # Evaluate the Model
    # Define the input function for evaluating
    input_fn = tf.estimator.inputs.numpy_input_fn(
        x={'images': mnist.test.images}, y=mnist.test.labels,
        batch_size=batch_size, shuffle=False)
    # Use the Estimator 'evaluate' method
    e = model.evaluate(input_fn)

    print("Testing Accuracy:", e['accuracy'])

class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=16,
                               kernel_size=5, stride=1, padding=0)
        self.fc1 = nn.Linear(in_features=576, out_features=128)
        self.fc2 = nn.Linear(in_features=128, out_features=10)

        nn.init.kaiming_normal_(self.conv1.weight)
        nn.init.kaiming_normal_(self.fc1.weight)
        nn.init.kaiming_normal_(self.fc2.weight)

    def forward(self, x):
        x = self.conv1(x)
        x = F.max_pool2d(x, kernel_size=4)
        x = F.relu(x)
        x = x.view(-1, 576)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)

def predict_batch(model, device, test_loader):
    examples = enumerate(test_loader)
    model.eval()
    with torch.no_grad():
        batch_idx, (data, target) = next(examples)
        data, target = data.to(device), target.to(device)
        output = model(data)
        pred = output.cpu().data.max(1, keepdim=True)[1] # get the index of the max log-probability
        pred = pred.numpy()
    return data, target, pred

def train_pytorch_model(model, device, train_loader, optimizer, epoch):
    model.train()
    correct=0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.cross_entropy(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
            pytorch_losses.append(loss.item())

            _, predicted = torch.max(output.data, 1)
            total = len(target)
            correct = (predicted == target).sum()
            accuracy = 100 * correct / total

            pytorch_accuracies.append(accuracy)
            # losses.append(loss.item())
            # counter.append((batch_idx*batch_size) + ((epoch-1)*len(train_loader.dataset)))
        pred = output.max(1, keepdim=True)[1]
        correct += pred.eq(target.view_as(pred)).sum().item()
    # errors.append(100. * (1 - correct / len(train_loader.dataset)))

def train_pytorch():
    use_cuda = torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    # data transformation
    train_data = datasets.MNIST('../data', train=True, download=True,
                   transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ]))
    test_data = datasets.MNIST('../data', train=False,
                   transform=transforms.Compose([
                       transforms.ToTensor(),
                       transforms.Normalize((0.1307,), (0.3081,))
                   ]))

    # data loaders
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=False, **kwargs)

	# extract and plot random samples of data
    examples = enumerate(test_loader)
    batch_idx, (data, target) = next(examples)

    # model creation
    model = CNN().to(device)
    # optimizer creation
    optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)

    # global training and testing loop
    for epoch in range(1, num_epochs + 1):
        start_time = time.time()
        train_pytorch_model(model, device, train_loader, optimizer, epoch)
        end_time = time.time()
        pytorch_times.append(end_time - start_time)

    # extract and plot random samples of data with predicted labels
    data, _, pred = predict_batch(model, device, test_loader)

def plot_loss():
    # present loss diagram
    plt.figure(1)
    plt.suptitle('Training Loss')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), keras_losses, color='blue')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), tf_losses, color='orange')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), pytorch_losses, color='red')
    plt.grid(True)
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.legend(('Keras', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    plt.show()

def plot_accuracy():
    plt.figure(2)
    plt.suptitle('Training Test Accuracy')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), keras_accuracies, color='blue')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), tf_accuracies, color='orange')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), pytorch_accuracies, color='red')
    plt.grid(True)
    plt.xlabel('Iteration')
    plt.ylabel('Accuracy (%')
    plt.legend(('Keras', 'TensorFlow', 'PyTorch'), loc='lower right', shadow=True)

    plt.show()

def plot_time():
    bar_width = 0.25

    plt.figure(3)
    plt.suptitle('Training Time Comsumption')

    plt.bar(np.arange(num_epochs), keras_times, bar_width, label="Keras")
    # plt.plot(index, sk_time_history, alpha=0.5, color='blue')
    # plt.scatter(index, sk_time_history, alpha=0.5, s=20, color='blue')

    plt.bar(np.arange(num_epochs) +  bar_width, tf_times, bar_width, label="TensorFlow")
    # plt.plot(index +  bar_width, tf_time_history, alpha=0.5, color='orange')
    # plt.scatter(index +  bar_width, tf_time_history, alpha=0.5, s=20, color='orange')

    plt.bar(np.arange(num_epochs) +  2 * bar_width, pytorch_times, bar_width, label="PyTorch")
    # plt.plot(index +  2 * bar_width, pytorch_time_history, alpha=0.5, color='red')
    # plt.scatter(index +  2 * bar_width, pytorch_time_history, alpha=0.5, s=20, color='red')

    plt.xlabel('Epoch')
    plt.ylabel('Time (s)')

    plt.grid(True)

    plt.legend(('Keras', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    plt.show()

if __name__ == '__main__':
    train_pytorch()
    train_tensorflow()
    train_keras()

    plot_loss()
    plot_accuracy()
    plot_time()
