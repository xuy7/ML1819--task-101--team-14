import numpy
import random
import numpy as np
from numpy import arange
#from classification import *
from sklearn import metrics, linear_model
from sklearn.datasets import fetch_mldata
from sklearn.utils import shuffle

input_size = 784
num_classes = 10
num_epochs = 25
batch_size = 100
learning_rate = 0.01

def batches(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]

def main():
    mnist = fetch_mldata('MNIST original', data_home='../data')
    #mnist.data, mnist.target = shuffle(mnist.data, mnist.target)
    #print mnist.data.shape
    # Trunk the data
    n_train = 60000
    n_test = 10000

    # Define training and testing sets
    indices = arange(len(mnist.data))
    random.seed(0)
    #train_idx = random.sample(indices, n_train)
    #test_idx = random.sample(indices, n_test)
    train_idx = arange(0,n_train)
    test_idx = arange(n_train+1,n_train+n_test)

    X_train, y_train = mnist.data[train_idx], mnist.target[train_idx]
    X_test, y_test = mnist.data[test_idx], mnist.target[test_idx]

    # Apply a learning algorithm
    print("Applying a learning algorithm...")
    clf = linear_model.SGDClassifier(loss="log", eta0=learning_rate)

    shuffledRange = list(range(n_train))

    for i in range(0, num_epochs):
        random.shuffle(shuffledRange)
        shuffledX = [X_train[i] for i in shuffledRange]
        shuffledY = [y_train[i] for i in shuffledRange]
        for batch in batches(range(n_train), batch_size):
            clf.partial_fit(shuffledX[batch[0]:batch[-1]+1], shuffledY[batch[0]:batch[-1]+1], classes=numpy.unique(y_train))

    # clf.partial_fit(X_train, y_train, classes=np.unique(y_train))
    # clf.fit(X_train, y_train)

    # Make a prediction
    print("Making predictions...")
    y_pred = clf.predict(X_test)

    #print y_pred

    # Evaluate the prediction
    print("Evaluating results...")
    print("Accuracy: \t", metrics.accuracy_score(y_test, y_pred))
    # print("Precision: \t", metrics.precision_score(y_test, y_pred, average='micro'))
    # print("Recall: \t", metrics.recall_score(y_test, y_pred))
    # print("F1 score: \t", metrics.f1_score(y_test, y_pred))
    # print("Mean accuracy: \t", clf.score(X_test, y_test))


if __name__ == "__main__":
    results = main()
