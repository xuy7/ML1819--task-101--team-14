import numpy
import random
import numpy as np
from numpy import arange
#from classification import *
from sklearn import metrics, linear_model
from sklearn.datasets import fetch_mldata
from sklearn.utils import shuffle

import tensorflow as tf

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

import os
import gzip
import six.moves.cPickle as pickle
import keras
from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.datasets import mnist
from keras.utils import np_utils

import matplotlib.pyplot as plt

import time

input_size = 784
num_classes = 10
num_epochs = 10
batch_size = 100
learning_rate = 0.01
log_interval = 20
n_train = 60000
n_test = 10000

keras_losses = []
sk_losses = []
tf_losses = []
pytorch_losses = []

keras_accuracies = []
sk_accuracies = []
tf_accuracies = []
pytorch_accuracies = []

keras_times = []
sk_times = []
tf_times = []
pytorch_times = []

def batches(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]

def train_keras():
    class LoggingCallback(keras.callbacks.Callback):
        def on_train_begin(self, logs={}):
            self.batch_idx = 0
            self.start_time = 0
            self.end_time = 0
            self.epoch = 0
            return

        def on_train_end(self, logs={}):
            return

        def on_epoch_begin(self, epoch, logs={}):
            self.epoch = epoch
            self.start_time = time.time();
            return

        def on_epoch_end(self, epoch, logs={}):
            self.end_time = time.time()
            keras_times.append(self.end_time - self.start_time)
            return

        def on_batch_begin(self, batch, logs={}):
            self.batch_idx += 1
            return

        def on_batch_end(self, batch, logs={}):
            if(self.batch_idx % log_interval == 0):
                loss = logs.get('loss')
                accuracy = logs.get('acc') * 100.
                # print("Loss: ", loss)
                # print("Accuracy: ", accuracy)
                # print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                #     (self.epoch+1), (self.batch_idx) * batch_size, n_train,
                #     100. * (self.batch_idx) * batch_size / len(X_train), loss))
                keras_losses.append(loss)
                keras_accuracies.append(accuracy)
            return

    def build_logistic_model(input_dim, output_dim):
        model = Sequential()
        model.add(Dense(output_dim, input_dim=input_dim, activation='softmax'))

        return model

    # the data, shuffled and split between train and test sets
    (X_train, y_train), (X_test, y_test) = mnist.load_data()

    X_train = X_train.reshape(60000, 784)
    X_test = X_test.reshape(10000, 784)
    X_train = X_train.astype('float32')
    X_test = X_test.astype('float32')
    X_train /= 255
    X_test /= 255
    print(X_train.shape[0], 'train samples')
    print(X_test.shape[0], 'test samples')

    logging_callback = LoggingCallback()

    # convert class vectors to binary class matrices
    Y_train = np_utils.to_categorical(y_train, num_classes)
    Y_test = np_utils.to_categorical(y_test, num_classes)

    model = build_logistic_model(784, num_classes)

    model.summary()

    # compile the model
    model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])
    history = model.fit(X_train, Y_train,
                        batch_size=batch_size, nb_epoch=num_epochs,
                        verbose=1, validation_data=(X_test, Y_test), callbacks=[logging_callback])
    score = model.evaluate(X_test, Y_test, verbose=0)

    print('Test score:', score[0])
    print('Test accuracy:', score[1])

def train_tensorflow():
    # Import MNIST data
    from tensorflow.examples.tutorials.mnist import input_data
    mnist = input_data.read_data_sets("../data/", one_hot=True)

    # tf Graph Input
    x = tf.placeholder(tf.float32, [None, 784]) # mnist data image of shape 28*28=784
    y = tf.placeholder(tf.float32, [None, 10]) # 0-9 digits recognition => 10 classes

    # Set model weights
    W = tf.Variable(tf.zeros([784, 10]))
    b = tf.Variable(tf.zeros([10]))

    # Construct model
    pred = tf.nn.softmax(tf.matmul(x, W) + b) # Softmax

    # Minimize error using cross entropy
    cost = tf.reduce_mean(-tf.reduce_sum(y*tf.log(pred), reduction_indices=1))

    acc, acc_op = tf.metrics.accuracy(labels=tf.argmax(y, 1), predictions=tf.argmax(pred, 1))

    # Gradient Descent
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    # Initialize the variables (i.e. assign their default value)
    init = tf.global_variables_initializer()
    init_local = tf.local_variables_initializer()

    # Start training
    with tf.Session() as sess:

        # Run the initializer
        sess.run(init)
        sess.run(init_local)

        # Training cycle
        for epoch in range(num_epochs):
            start_time = time.time()
            avg_cost = 0.
            total_train_batch = int((mnist.train.num_examples) / batch_size)
            total_validation_batch = int((mnist.validation.num_examples) / batch_size)
            total_batch = total_train_batch + total_validation_batch
            # Loop over all batches
            for i in range(total_train_batch):
                batch_xs, batch_ys = mnist.train.next_batch(batch_size)
                # Run optimization op (backprop) and cost op (to get loss value)
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_xs,
                                                              y: batch_ys})
                # Compute average loss
                avg_cost += c / total_batch

                if i % log_interval == 0:
                    sess.run([acc, acc_op], feed_dict={x: mnist.test.images, y: mnist.test.labels})
                    accuracy = sess.run(acc, feed_dict={x: mnist.test.images, y: mnist.test.labels}) * 100.

                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                        epoch, (i) * batch_size, mnist.train.num_examples + mnist.validation.num_examples,
                        100. * i / total_batch, c))
                    tf_losses.append(c)
                    tf_accuracies.append(accuracy)

            for j in range(total_validation_batch):
                sess.run([acc, acc_op], feed_dict={x: mnist.test.images, y: mnist.test.labels})
                accuracy = sess.run(acc, feed_dict={x: mnist.test.images, y: mnist.test.labels}) * 100.

                batch_xs, batch_ys = mnist.validation.next_batch(batch_size)
                # Run optimization op (backprop) and cost op (to get loss value)
                _, c = sess.run([optimizer, cost], feed_dict={x: batch_xs,
                                                              y: batch_ys})
                # Compute average loss
                avg_cost += c / total_batch

                sess.run([acc, acc_op], feed_dict={x: mnist.test.images, y: mnist.test.labels})
                accuracy = sess.run(acc, feed_dict={x: mnist.test.images, y: mnist.test.labels}) * 100.

                if (j + total_train_batch) % log_interval == 0:
                    print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                        epoch, (j + total_train_batch) * batch_size, mnist.train.num_examples + mnist.validation.num_examples,
                        100. * (j + total_train_batch) / total_batch, c))
                    tf_losses.append(c)
                    tf_accuracies.append(accuracy)
            print("Epoch:", '%d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost))
            end_time = time.time()
            tf_times.append(end_time - start_time)

        print("Optimization Finished!")

        # Test model
        correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
        # Calculate accuracy
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        print("Accuracy:", accuracy.eval({x: mnist.test.images, y: mnist.test.labels}))

def train_pytorch():
    # MNIST dataset (images and labels)
    train_dataset = torchvision.datasets.MNIST(root='../data',
                                               train=True,
                                               transform=transforms.ToTensor(),
                                               download=True)

    test_dataset = torchvision.datasets.MNIST(root='../data',
                                              train=False,
                                              transform=transforms.ToTensor())

    # Data loader (input pipeline)
    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True)

    test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                              batch_size=batch_size,
                                              shuffle=False)

    # Logistic regression model
    model = nn.Linear(input_size, num_classes)

    # Loss and optimizer
    # nn.CrossEntropyLoss() computes softmax internally
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

    # Train the model
    total_step = len(train_loader)
    for epoch in range(num_epochs):
        start_time = time.time()
        total_batch = total_step
        avg_cost = 0.

        for i, (images, labels) in enumerate(train_loader):
            # Reshape images to (batch_size, input_size)
            images = images.reshape(-1, 28*28)

            # Forward pass
            outputs = model(images)
            loss = criterion(outputs, labels)

            # Backward and optimize
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            avg_cost += loss.item() / total_batch

            if i % log_interval == 0:
                _, predicted = torch.max(outputs.data, 1)
                total = len(labels)
                correct = (predicted == labels).sum()
                accuracy = 100 * correct / total

                print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    (epoch+1), (i) * batch_size, total_step * batch_size,
                    100. * i / total_step, loss.item()))
                pytorch_losses.append(loss.item())
                pytorch_accuracies.append(accuracy)

        print("Epoch:", '%d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost))
        end_time = time.time()
        pytorch_times.append(end_time - start_time)

    # Test the model
    # In test phase, we don't need to compute gradients (for memory efficiency)
    with torch.no_grad():
        correct = 0
        total = 0
        for images, labels in test_loader:
            images = images.reshape(-1, 28*28)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum()

        print('Accuracy of the model on the 10000 test images: {} %'.format(100. * correct / total))

def plot_loss():
    # present loss diagram
    plt.figure(1)
    plt.suptitle('Training Loss')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), keras_losses, color='blue')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), tf_losses, color='orange')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), pytorch_losses, color='red')
    plt.grid(True)
    plt.xlabel('Iteration')
    plt.ylabel('Loss')
    plt.legend(('Keras', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    plt.show()

def plot_accuracy():
    plt.figure(2)
    plt.suptitle('Training Test Accuracy')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), keras_accuracies, color='blue')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), tf_accuracies, color='orange')
    plt.plot(range(1, (n_train * num_epochs) + 1, batch_size * log_interval), pytorch_accuracies, color='red')
    plt.grid(True)
    plt.xlabel('Iteration')
    plt.ylabel('Accuracy (%)')
    plt.legend(('Keras', 'TensorFlow', 'PyTorch'), loc='lower right', shadow=True)

    plt.show()

def plot_time():
    bar_width = 0.25

    plt.figure(3)
    plt.suptitle('Training Time Comsumption')

    plt.bar(np.arange(num_epochs), keras_times, bar_width, label="Scikit-Learn")
    # plt.plot(index, sk_time_history, alpha=0.5, color='blue')
    # plt.scatter(index, sk_time_history, alpha=0.5, s=20, color='blue')

    plt.bar(np.arange(num_epochs) +  bar_width, tf_times, bar_width, label="TensorFlow")
    # plt.plot(index +  bar_width, tf_time_history, alpha=0.5, color='orange')
    # plt.scatter(index +  bar_width, tf_time_history, alpha=0.5, s=20, color='orange')

    plt.bar(np.arange(num_epochs) +  2 * bar_width, pytorch_times, bar_width, label="PyTorch")
    # plt.plot(index +  2 * bar_width, pytorch_time_history, alpha=0.5, color='red')
    # plt.scatter(index +  2 * bar_width, pytorch_time_history, alpha=0.5, s=20, color='red')

    plt.xlabel('Epoch')
    plt.ylabel('Time (s)')

    plt.grid(True)

    plt.legend(('Keras', 'TensorFlow', 'PyTorch'), loc='upper right', shadow=True)

    plt.show()

if __name__ == "__main__":
    print('''
        ========================================================================
        =                       Keras Train Starts
        ========================================================================
    ''')

    train_keras()

    print('''
        ========================================================================
        =                       keras Train Ends
        ========================================================================
    ''')

    print('''\n\n\n
        ========================================================================
        =                       TensorFlow Train Starts
        ========================================================================
    ''')

    train_tensorflow()

    print('''
        ========================================================================
        =                       TensorFlow Train Ends
        ========================================================================
    ''')

    print('''\n\n\n
        ========================================================================
        =                       PyTorch Train Starts
        ========================================================================
    ''')

    train_pytorch()

    print('''
        ========================================================================
        =                       PyTorch Train Ends
        ========================================================================
    ''')

    plot_loss()
    plot_accuracy()
    plot_time()
